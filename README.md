# Timetables

Timetables is an application offering filtered views from Google Calendars (teachers, classes, rooms, etc.). Another feature is the capacity to audit the amount of training hours for a teacher during a given module.

## Backup

Please note that this application doesn't use a database. It relies on few JSON files as we don't frequently update the information. These files are stored into `backend/data`.

TODO: Access to the documentation

## Deployment

### Prequisites

You need the following software:
* PHP Cli for Composer and the optional artisan commands.
* Git to clone this repository.
* NodeJS so as to use NPM or yarn package manager.
* Composer so as to install PHP 3rd party libraries

### Instructions

Deployment on a webserver:

You need two DNS entries, for example:
 - Backend api-timetables.pnc.passerellesnumeriques.org
 - Frontend timetables.pnc.passerellesnumeriques.org

The backend should be served via TLS with Let's Encrypt for example.

Now imagine you clone this repositiory into /var/www/timetables/

The Backend location folder will be /var/www/timetables/backend/public
The Frontend location folder will be /var/www/timetables/frontend/build

The configuration of the frontend for production: pnc-timetables/frontend/config/prod.env.js

Test of the public API:
https://api-timetables.pnc.passerellesnumeriques.org/api/v1/calendar/teachers

Additional steps are require in order to deploy the application.

1. Into the backend subfolder, run the command `composer install --no-dev`
2. Into the frontend subfolder, apply the procedure of the README file. For production, it mainly consists in executing the following commands:

    $ npm install
    $ npm run build

### Nginx webserver

#### Sample config for frontend

    server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        server_name test.pnc.passerellesnumeriques.org;
        root /var/www/pnc-timetables/frontend/dist;
        index index.html;
        location / {
            try_files $uri $uri/ /index.html;
        }
        #Exclude htaccess here
        #SSL certificates here
    }

#### Sample config for backend

    server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        server_name api-timetables.pnc.passerellesnumeriques.org;
        root /var/www/pnc-timetables/backend/public;
        index index.php;
        location / {
            try_files $uri $uri/ /index.php?$query_string;
        }

        # pass PHP scripts to FastCGI server
        location ~ \.php$ {
            include snippets/fastcgi-php.conf;
            fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_param HTTP_AUTHORIZATION $http_authorization;
        }
        #Exclude htaccess here
        #SSL certificates here
    }


### Configuration of backend

Admin users and passwords:
backend/data/admin.json

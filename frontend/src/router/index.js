import Vue from 'vue';
import Router from 'vue-router';
import Calendar from '@/pages/Calendar';
import Print from '@/pages/Print';
import Login from '@/pages/Login';
import Statistics from '@/pages/admin/Statistics';
import Modules from '@/pages/admin/Modules';
import Lessons from '@/pages/admin/Lessons';
import Teachers from '@/pages/admin/Teachers';
import Classes from '@/pages/admin/Classes';
import Rooms from '@/pages/admin/Rooms';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Calendar Default',
      component: Calendar
    },
    {
      // Type should be students, rooms, teachers
      path: '/calendar/:type?',
      name: 'Calendar',
      component: Calendar
    },
    {
      // Type should be classes, rooms, teachers
      path: '/print/:type?',
      name: 'Print',
      component: Print
    },
    {
      path: '/login/',
      name: 'Login',
      component: Login
    },
    {
      // This page require to be authenticated
      path: '/admin/stats/',
      name: 'Statistics',
      component: Statistics
    },
    {
      // This page require to be authenticated
      path: '/admin/modules/',
      name: 'Modules',
      component: Modules
    },
    {
      // This page require to be authenticated
      path: '/admin/lessons/',
      name: 'Lessons',
      component: Lessons
    },
    {
      // This page require to be authenticated
      path: '/admin/teachers/',
      name: 'Teachers',
      component: Teachers
    },
    {
      // This page require to be authenticated
      path: '/admin/rooms/',
      name: 'Rooms',
      component: Rooms
    },
    {
      // This page require to be authenticated
      path: '/admin/classes/',
      name: 'Classes',
      component: Classes
    }
  ]
});

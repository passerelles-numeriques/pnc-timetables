// Class used for autentication
export default class AdminService {
  constructor(context) {
    this.context = context;
  }

  isAdmin() {
    return this.context.$session.exists();
  }
  getName() {
    return this.context.$session.get('username');
  }

  getPassword() {
    return this.context.$session.get('password');
  }
  getCredentials() {
    return `user=${this.getName()}&password=${this.getPassword()}`;
  }

  // Is used when a http request need an authentification
  adminFetch(url, callback, fetchOption = null) {
    if (!fetchOption) {
      fetchOption = { mode: 'cors' };
    }
    fetch(
      `${url}?${this.getCredentials()}`,
      fetchOption
    )
      .then(response => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw response.text();
        }
      })
      .then(json => {
        callback(json);
      })
      .catch(e => {
        e.then(ex => console.log(ex));
      });
  }
}

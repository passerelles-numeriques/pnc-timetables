'use strict';
module.exports = {
  NODE_ENV: '"production"',
  BACKEND_ENDPOINT: '"https://api-timetables.pnc.passerellesnumeriques.org/api/v1"'
};

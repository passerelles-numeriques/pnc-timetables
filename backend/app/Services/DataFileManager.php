<?php

namespace App\Services;

use App\Models\Enum\DataFileEnum;

class DataFileManager
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the content of a json file
     *
     * @param string $fileName
     * @param boolean $isBackup
     * @return mixed  content of the file
     */
    public static function getDataFile($fileName, $isBackup = false)
    {
        $file_path = DataFileManager::getDataFilePath($fileName, $isBackup);
        $jsonData = json_decode(file_get_contents($file_path));
        if($jsonData === null) {
            $backup = DataFileManager::getDataFile($fileName, true);
            $file_path = DataFileManager::getDataFilePath($fileName);
            file_put_contents($file_path,json_encode($backup));
            return $backup;
        }
        return $jsonData;
    }

    /**
     * Write content into json file
     *
     * @param string $fileName
     * @param mixed $content
     * @return boolean
     */
    public static function writeDataFile($fileName, $content) {
        $contentToBackup = DataFileManager::getDataFile($fileName);
        $file_path = DataFileManager::getDataFilePath($fileName, true);
        $result = file_put_contents($file_path,json_encode($contentToBackup));
        $file_path = DataFileManager::getDataFilePath($fileName);
        $result = file_put_contents($file_path,json_encode($content));
        return $result != false;
    }

    /**
     * Get the full path of a file
     *
     * @param string $fileName
     * @param boolean $isBackup
     * @return string
     */
    private static function getDataFilePath($fileName, $isBackup = false){
      if (!DataFileEnum::isADataFile($fileName)) {
          throw new \Exception("The data file doesn't exist\nYou should use App\Models\Enum\DataFileEnum instead", 1);
      }
      return $file_path = realpath(__DIR__ . '/../../data/' . $fileName . ($isBackup ? '.backup' : '') . '.json');
    }
}
